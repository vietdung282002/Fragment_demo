package com.example.fragment_demo


import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.example.fragment_demo.databinding.FragmentaBinding

class FragmentA : Fragment(R.layout.fragmentb) {
    private lateinit var seekbar: SeekBar
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var runnable: Runnable
    private var handler = Handler()
    private var _binding: FragmentaBinding? = null
    private val binding get() = _binding!!
    private var current: Int = 0
    private lateinit var fragmentManager: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"Fragment A onCreate")
        if (savedInstanceState != null) {
            current = savedInstanceState.getInt("current")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG,"Fragment A OnCreateView")
        _binding = FragmentaBinding.inflate(inflater,container,false)
        val view = binding.root
        mediaPlayer = MediaPlayer.create(activity,R.raw.a)
        mediaPlayer.seekTo(current)
        seekbar = binding.seekbar2
        seekbar.max = mediaPlayer.duration
        seekbar.progress = mediaPlayer.currentPosition
        fragmentManager = requireActivity().supportFragmentManager

        binding.button.setOnClickListener {
            fragmentManager.commit {
                setReorderingAllowed(true)
                replace<FragmentB>(R.id.fragment_container_view_a)
                addToBackStack(null)
                Log.d(TAG,"Replace Fragment A by Fragment B")
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        runnable = Runnable {
            seekbar.progress = mediaPlayer.currentPosition
            handler.postDelayed(runnable,1000)
        }
        handler.postDelayed(runnable,1000)
        mediaPlayer.setOnCompletionListener {
            mediaPlayer.pause()
        }
    }


    override fun onStart() {
        super.onStart()
        Log.d(TAG,"Fragment A onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"Fragment A onResume")
        mediaPlayer.start()
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer.pause()
        Log.d(TAG,"Fragment A onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"Fragment A onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stop()
        Log.d(TAG,"Fragment A onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG,"Fragment A onCreate")
        _binding = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // Lưu bộ đếm vào Bundle
        outState.putInt("current", mediaPlayer.currentPosition)
    }
}