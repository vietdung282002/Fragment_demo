package com.example.fragment_demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import android.content.Intent
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.fragment.app.FragmentManager
import com.example.fragment_demo.databinding.ActivityMainBinding



const val TAG = "Fragment Lifecycle"

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<FragmentA>(R.id.fragment_container_view_a)
                addToBackStack(null)
                Log.d(TAG,"Add Fragment A")
            }
        }

    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG,"Activity A onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"Activity A onResume")
    }
    override fun onRestart() {
        super.onRestart()
        Log.d(TAG,"Activity A Restart")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG,"Activity A onPause ")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"Activity A onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"Activity A onDestroy")
    }
}