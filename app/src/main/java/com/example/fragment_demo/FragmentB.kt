package com.example.fragment_demo

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.FragmentManager
import com.example.fragment_demo.databinding.FragmentbBinding


class FragmentB : Fragment(R.layout.fragmentb) {
    private lateinit var seekbar: SeekBar
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var runnable: Runnable
    private var handler =  Handler()
    private var _binding: FragmentbBinding? = null
    private val binding get() = _binding!!
    private var current: Int = 0
    private lateinit var fragmentManager: FragmentManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"Fragment B onCreate")
        if (savedInstanceState != null) {
            current = savedInstanceState.getInt("current")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG,"Fragment B OnCreateView")
        _binding = FragmentbBinding.inflate(inflater,container,false)
        val view = binding.root
        mediaPlayer = MediaPlayer.create(activity,R.raw.b)
        mediaPlayer.seekTo(current)
        seekbar = binding.seekbar1
        seekbar.max = mediaPlayer.duration
        seekbar.progress = mediaPlayer.currentPosition

        fragmentManager = requireActivity().supportFragmentManager
        binding.button1.setOnClickListener {
            fragmentManager.popBackStack()
            Log.d(TAG,"Pop Fragment B out of Stack")
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG,"Fragment B onViewCreated")

        mediaPlayer.start()

        runnable = Runnable {
            seekbar.progress = mediaPlayer.currentPosition
            handler.postDelayed(runnable,1000)
        }
        handler.postDelayed(runnable,1000)
        mediaPlayer.setOnCompletionListener {
            mediaPlayer.pause()
        }
    }



    override fun onStart() {
        super.onStart()
        Log.d(TAG,"Fragment B onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"Fragment B onResume")
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer.pause()
        Log.d(TAG,"Fragment B onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"Fragment B onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stop()
        Log.d(TAG,"Fragment B onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG,"Fragment B onDestroyView")
        _binding = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // Lưu bộ đếm vào Bundle
        outState.putInt("current", mediaPlayer.currentPosition)
    }
}